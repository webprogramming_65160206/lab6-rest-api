import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { TemperatureService } from './temperature.service';

@Controller('temperature')
export class TemperatureController {
  // inject service ตรงนี้ฮาฟฟู่ววว
  constructor(private readonly temperatureService: TemperatureService) {}
  @Get('convert')
  convert(@Query('celsius') celsius: string) {
    return this.temperatureService.convert(parseFloat(celsius));
  }

  // post ต้องใช้คู่กับbody ตัว นี้จะ post หรือ ดึงข้อมูลให้ดึงเป็น body ใน post และส่งค่าเป็นjson (พิมพ์สด)
  @Post('convert')
  convertByPost(@Body('celsius') celsius: number) {
    return this.temperatureService.convert(celsius);
  }

  // param ต่าง จาก query ตรงที่param จะกำหนด url ที่ชัดเจนเวลาจะget
  @Get('convert/:celsius')
  convertParam(@Param('celsius') celsius: string) {
    return this.temperatureService.convert(parseFloat(celsius));
  }
}
