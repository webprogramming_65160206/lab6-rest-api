import { Injectable } from '@nestjs/common';

// บรรทัดข้างล่างเรียกว่า Annotation
@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }
}
