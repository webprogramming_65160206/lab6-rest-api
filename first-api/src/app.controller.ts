import { Body, Controller, Get, Param, Post, Query, Req } from '@nestjs/common';
import { AppService } from './app.service';
import { get } from 'http';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getDefault(): string {
    return 'Default';
  }

  // ถ้าเราใส่อะไรเข้าไปในGet ก็จะต้องเรียกบนเว็บ ตามpath ที่ใส่ไป
  @Get('hello')
  getHello(): string {
    return '<html><body><h1>Hello Buu</h1></body></html>';
  }
  //annotation will link with postman if you change it will also change
  @Post('world')
  getWorld(): string {
    return '<html><body><h1>Buu World</h1></body></html>';
  }

  @Get('test-query')
  TestQuery(
    @Req() req,
    @Query('celsius') celsius: number,
    @Query('type') type: string,
  ) {
    return {
      celsius: celsius,
      type: type,
    };
  }

  @Get('test-params/:celsius')
  TestParam(@Req() req, @Param('celsius') celsius: number) {
    return {
      celsius,
    };
  }

  // กำหนดพวก get post patch put delete
  @Post('test-body')
  // ชื่อmethod และ req ตามในdocument ในวงเล็บของparam and body need to be theเซม  ทูเมคอิทแมพ
  TestBody(@Req() req, @Body('celsius') celsius: number) {
    return { celsius };
  }
}
